import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Asset {
  static Color colorPrimaryDark = Color.fromARGB(255, 2, 84, 100);
  static Color colorPrimary = Color.fromARGB(255, 229, 124, 35);
  static Color colorSecondary = Color.fromARGB(255, 232, 170, 66);
  static Color colorAccent = Color.fromARGB(255, 229, 124, 35);
}
